<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" 
    crossorigin="anonymous">
    <title>Data Nilai </title>
    <style>
    .form{
      color :white;
      position: center; 
      padding: 10% 25%; 
      background-color: black;    
    }
    .form .b{
      position: absolute;
      left: 640px;
    }
    </style>
  </head>
  <body>
  <form action="output.php" method= "post" >
  <div class="form">
  <h1><b> Input Nilai </b></h1>
      <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">
        <b>Nama :</label></b>
        <div class= "col-sm-10">
        <input  type="text" name="input_nama"class="form-control-sm"><br>
        </div>
        
      <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">
        <b>Mata Pelajaran :</label><br></b>
        <input type="radio" name="radio_pelajaran" value="Bahasa Indonesia">Bahasa Indonesia <br>
        <input type="radio" name="radio_pelajaran" value="Bahasa Inggris">Matematika <br>
        <input type="radio" name="radio_pelajaran" value="Bahasa Madura">Bahasa Inggris <br>
        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">
        <b>Nilai UTS :</label></b>
        <div class="col-sm-10">
        <input type="text" name="input_uts" class="form-control-sm"><br>
        </div>
        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">
        <b>Nilai UAS :</label></b>
        <div class="col-sm-10">
        <input type="text" name="input_uas" class="form-control-sm"><br>
        </div>
        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm">
        <b>Nilai Tugas : </label></b>
        <div class="col-sm-10">
        <input type="text" name="input_tugas" class="form-control-sm"><br>
        </div>
        <div class= "b">
    <input type="submit" value="Simpan" class="btn btn-primary">
    </div>
        
    </div>
    
      </form>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>