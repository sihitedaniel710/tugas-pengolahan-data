<!DOCTYPE html>
<html>
<head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" 
    crossorigin="anonymous">
    <title>Laporan</title>
    <style>
        body{
            color: white;
            position: absolute 100%; 
            padding:10% 28%; 
            background-color: black;    
        }
        /* .output{
            position: absolute;
            left: 640px;
        } */
    </style>
</head>
<body>
<div class= output>
    <h1><b> Laporan Hasil </b></h1>
    <?php 
        $nama = $_POST["input_nama"];
        $pelajaran = $_POST["radio_pelajaran"];
        $uts = $_POST["input_uts"];
        $uas = $_POST["input_uas"];
        $tugas = $_POST["input_tugas"];

        $nilai = ($tugas*0.15)+($uas*0.5)+($uts*0.35);
        if($nilai >=90){
            $grade = "A";
        }elseif($nilai >=70){
            $grade = "B";
        }elseif($nilai >=50){
            $grade = "C";
        }else{
            $grade ="D";
        }
        echo "Siswa/i yang bernama $nama dalam Mata Pelajaran $pelajaran mendapat : <br>"; 
        echo "Nilai UTS  : $uts <br> Nilai UAS : $uas <br> rata-rata : $nilai <br> Predikat : $grade";
    ?>  
</div>
</body>
</html>